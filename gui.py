﻿import pycountry
from tkinter import *
from tkinter import messagebox
from tkinter import ttk


COUNTRY_LIST = list(pycountry.countries)
countries = [country.name for country in COUNTRY_LIST]
from get_delivery_price import get_box_price, get_parcel_price, get_small_package_price, get_zone_country, get_recommended_parcel_price


def get_country_code(country):
    for country_obj in COUNTRY_LIST:
        if country == country_obj.name:
            return country_obj.alpha_2 


class Window(Canvas):
    def __init__(self, master=None):
        Canvas.__init__(self, master)
        self.master = master
        self.init_window()

    def build_result_response(self, box_price, parcel_price,recommended_parcel_price, small_package_price):
        result = ''
        if isinstance(box_price, str):
            result += 'Посилка - %s' % box_price
        else:
            result += 'Посилка - %0.2f грн' % box_price
        if isinstance(parcel_price, str):
            result += '\nБандероль - %s' % parcel_price
        else:
            result += '\nБандероль - %0.2f грн' % parcel_price
        if isinstance(recommended_parcel_price, str):
            result += '\nБандероль(рек.) - %s' % recommended_parcel_price
        else:
            result += '\nБандероль(рек.) - %0.2f грн' % recommended_parcel_price
        if isinstance(small_package_price, str):
            result += '\nДрібний пакет - %s' % small_package_price
        else:
            result += '\nДрібний пакет - %0.2f грн' % small_package_price
        return result
    
    def get_shipping_prices(self):
        mass_kg = int(self.mass_kg.get(1.0, END).strip() or '0')
        mass_g = int(self.mass_g.get(1.0, END).strip() or '0')
        country = countries[self.country_listbox.current()]
        country_code = get_country_code(country)
        zone = get_zone_country(country_code)
        box_price = get_box_price(country_code, mass_kg, mass_g)
        parcel_price = get_parcel_price(country_code, mass_kg, mass_g)
        recommended_parcel_price = get_recommended_parcel_price(country_code, mass_kg, mass_g)
        small_package_price = get_small_package_price(zone, mass_kg, mass_g)
        result = self.build_result_response(box_price, parcel_price, recommended_parcel_price, small_package_price)
        messagebox.showinfo('Result', result)
        
    def init_window(self):
        self.master.title("Обчислення ціни доставки")
        self.mass_kg_label = Label(self, text='Маса, кг')
        self.mass_kg = Text(self, width=10, height=1)
        self.mass_g = Text(self, width=10, height=1)
        self.mass_g_label = Label(self, text='Маса, г')
        self.country_listbox = ttk.Combobox(self, values=countries)
        self.button = Button(self,text='Отримати результат', command=self.get_shipping_prices)
        self.mass_kg.place(x=40, y=50)
        self.mass_kg_label.place(x=40, y=30)
        self.mass_g_label.place(x=160, y=30)
        self.mass_g.place(x=160, y=50)
        self.country_listbox.place(x=40, y=100)
        self.button.place(x=200, y=200)
        self.pack(fill=BOTH, expand=1)

root = Tk()
root.geometry("400x300")
app = Window(root)

root.mainloop()
    
