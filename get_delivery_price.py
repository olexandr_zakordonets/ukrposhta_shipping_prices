﻿import requests
import json

PACKAGE_ZONES = {
    '1': ['AT', 'AZ', 'AL', 'DZ', 'AD', 'AF', 'BH', 'BA', 'BG', 'BE', 'BY', 'VA', 'GB', 'AM', 'GI', 'GR', 'GL', 'GE', 'DK', 'EE','ES', 'YE', 'EG', 'EH', 'IL', 'IQ', 'IR', 'IT', 'IE', 'IS', 'JO', 'KZ', 'KG', 'QA', 'CY', 'KW', 'LV', 'LT', 'LB', 'LY', 'LI', 'LU', 'MK', 'MA', 'MT', 'MD', 'MC', 'DE', 'NO', 'NL', 'AE', 'OM', 'PS', 'PL', 'PT', 'RU', 'RO', 'SM', 'SA', 'SY', 'RS', 'SK', 'SI', 'SD', 'TN', 'TR', 'TJ', 'TM', 'HU', 'UZ', 'FR', 'FI', 'HR', 'CZ', 'ME', 'CH', 'SE', 'FO'], 
    '2': ['HK', 'CA', 'CN', 'MN', 'TW', 'JP', 'KP', 'KR', 'US'], 
    '3': ['AI', 'AO', 'AG', 'BQ', 'AR', 'AW', 'AU', 'AS', 'AQ', 'BB', 'BT', 'BD', 'BZ', 'BJ', 'BO', 'BW', 'BR', 'BF', 'BI', 'VU', 'VN', 'VE', 'GA', 'HT', 'GY', 'GM', 'GH', 'GP', 'GT', 'GN', 'GW', 'HN', 'GD', 'GU', 'CD', 'IO', 'DM', 'DO', 'DJ', 'EC', 'GQ', 'ER', 'ET', 'WS', 'ZM', 'ZW', 'ID', 'IN', 'CV', 'KH', 'CM', 'KE', 'KI', 'CO', 'CG', 'CU', 'CW', 'LA', 'LS', 'LR', 'MY', 'MU', 'MR', 'MG', 'YT', 'MW', 'ML', 'MV', 'MQ', 'MX', 'MZ', 'MS', 'MN', 'NA', 'NP', 'NR', 'NU', 'NZ', 'NC', 'NF', 'NE', 'NG', 'NI', 'CK', 'CX', 'PK', 'PA', 'PG', 'PY', 'PE', 'ZA', 'PR', 'PW', 'PN', 'RE', 'RW', 'ST', 'SZ', 'SN', 'VC', 'KN', 'LC', 'SG', 'SX', 'SB', 'SO', 'SR', 'SL', 'TZ', 'TH', 'TG', 'TT', 'TK', 'TO', 'TV', 'UG', 'UY', 'PH', 'PF', 'GF', 'TD', 'LK', 'JM', 'BS', 'BN', 'BM', 'VG', 'VI', 'CC', 'KM', 'CR', 'CI', 'MH', 'FM', 'MO', 'MP', 'KY', 'SH', 'SS', 'SV', 'SC', 'PM', 'TC', 'TL', 'WF', 'FJ', 'FK', 'CF', 'CL']}


class ValidationError(Exception):
    pass


def validate_mass(value_kg, value_g, max_mass):
    """
    Simple function to validate mass.
    """
    if (value_kg+value_g/1000) > max_mass:
        raise ValidationError('Package mass exceeds max mass for this type')


def get_shipping_price(country_code, data):
    """
    Basic function to retrieve shipping price for Ukrposhta.
    Note that data to send for small packages is different from data for boxes and parcels
    """
    response = requests.post('https://a.ukrposhta.ua/_g/s/library/Calculator/CalculatorController.php', data=data)
    try:
        result = json.loads(response.text)
        return result['sum']/100
    except json.JSONDecodeError:
        return 'Could not get shipping price info from Ukrposhta'


def get_box_price(country_code, mass_kg, mass_g):
    try:
        validate_mass(mass_kg, mass_g, 30)
    except ValidationError:
        return 'Маса вантажу перевищує максимальну масу для цього типу'
    data={'type_of_departure': 'box',
        'departure_category':'simple',
        'departure_type':'physical',
        'destination':'foreign_countries',
        'destination_country':country_code,
        'transportation_method':'aviation', 
        'mass_kg':mass_kg, 
        'mass_c':mass_g}
    return get_shipping_price(country_code=country_code, data=data)


def get_parcel_price(country_code, mass_kg, mass_g):
    try:
        validate_mass(mass_kg, mass_g, 5)
    except ValidationError:
        return 'Маса вантажу перевищує максимальну масу для цього типу'
    data={'type_of_departure': 'parcel',
        'departure_category':'simple',
        'departure_type':'physical',
        'destination':'foreign_countries',
        'destination_country':country_code,
        'transportation_method':'aviation', 
        'mass_kg':mass_kg, 
        'mass_c':mass_g}
    return get_shipping_price(country_code=country_code, data=data)

def get_recommended_parcel_price(country_code, mass_kg, mass_g):
    try:
        validate_mass(mass_kg, mass_g, 5)
    except ValidationError:
        return 'Маса вантажу перевищує максимальну масу для цього типу'
    data={'type_of_departure': 'parcel',
        'departure_category':'recommended',
        'departure_type':'physical',
        'destination':'foreign_countries',
        'destination_country':country_code,
        'transportation_method':'aviation', 
        'mass_kg':mass_kg, 
        'mass_c':mass_g}
    return get_shipping_price(country_code=country_code, data=data)


def get_zone_country(country_code):
    for key in PACKAGE_ZONES:
        if country_code in PACKAGE_ZONES[key]:
            return key


def get_small_package_price(zone_code, mass_kg, mass_g):
    try:
        validate_mass(mass_kg, mass_g, 2)
    except ValidationError:
        return 'Маса вантажу перевищує максимальну масу для цього типу'
    response = requests.post('https://a.ukrposhta.ua/_g/s/library/Calculator/CalculatorController.php',
        data={'type_of_departure': 'small_package',
         'departure_category':'recommended',
         'departure_type':'physical',
         'destination':'foreign_countries',
         'destination_country_small':zone_code,
         'transportation_method':'aviation',
         'mass_kg':mass_kg, 
         'mass_c':mass_g})
    try:
        result = json.loads(response.text)
        return result['sum']/100
    except json.JSONDecodeError:
        return 'Could not get shipping price info from Ukrposhta'
